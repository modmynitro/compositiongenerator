﻿using System.Collections.Immutable;
using System.Text;
using CompositionGenerator.Attributes;
using CompositionGenerator.Extensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;

namespace CompositionGenerator.SourceGenerators;

/// <summary>
/// https://andrewlock.net/creating-a-source-generator-part-1-creating-an-incremental-source-generator/
/// </summary>
[Generator]
public class CompositionSourceGenerator : IIncrementalGenerator
{
    public void Initialize(IncrementalGeneratorInitializationContext context)
    {
        // Add the marker attribute to the compilation
        context.RegisterPostInitializationOutput(ctx =>
            ctx.AddSource("CompositionAttribute`1.g.cs",
                SourceText.From(CompositionAttributes.Attribute, Encoding.UTF8)));

        // Do a simple filter for enums
        var classDeclaration = context.SyntaxProvider
            .CreateSyntaxProvider(
                predicate: static (s, _) => IsSyntaxTargetForGeneration(s),
                transform: static (ctx, _) =>
                    GetSemanticTargetForGeneration(ctx))
            .Where(static m => m is not null);

        // Combine the selected enums with the `Compilation`
        var compilationAndEnums
            = context.CompilationProvider.Combine(classDeclaration.Collect());

        context.RegisterSourceOutput(compilationAndEnums,
            static (spc, source) => Execute(source.Left, source.Right, spc));
    }

    private static bool IsSyntaxTargetForGeneration(SyntaxNode node)
    {
        if (node is FieldDeclarationSyntax fieldDeclarationSyntax)
            return fieldDeclarationSyntax.AttributeLists.Count > 0;

        return false;
    }

    private static Target? GetSemanticTargetForGeneration(GeneratorSyntaxContext context)
    {
        var fieldDeclarationSyntax = (FieldDeclarationSyntax)context.Node;
        var classDeclarationSyntax = fieldDeclarationSyntax.TryGetParent<ClassDeclarationSyntax>();

        if (classDeclarationSyntax is null || !classDeclarationSyntax.IsPartial())
            return null;

        var classTypeSymbol = context.SemanticModel.GetTypeInfo(classDeclarationSyntax).Type;

        var fieldName = string.Empty;
        var compositions = new List<INamedTypeSymbol>();

        // loop through all the attributes on the method
        foreach (var attributeListSyntax in fieldDeclarationSyntax.AttributeLists)
        {
            if (fieldDeclarationSyntax.Declaration.Variables.Count > 1)
                continue;

            fieldName = fieldDeclarationSyntax.Declaration.Variables[0].Identifier.Text;

            foreach (var attributeSyntax in attributeListSyntax.Attributes)
            {
                var symbolInfo = context.SemanticModel.GetTypeInfo(attributeSyntax);

                if (symbolInfo.Type is not INamedTypeSymbol attributeSymbol)
                    continue;

                var fullName = attributeSymbol.ConstructedFrom.ToDisplayString();

                if (fullName is "CompositionGenerator.Attributes.CompositionAttribute<TInterface>" &&
                    attributeSymbol.TypeArguments[0] is INamedTypeSymbol typeSymbol)
                    compositions.Add(typeSymbol);
            }
        }

        if (compositions.Count == 0)
            return null;

        return new(
            (INamedTypeSymbol)classTypeSymbol,
            fieldName,
            compositions.Distinct(SymbolEqualityComparer<INamedTypeSymbol>.Default).ToList());
    }

    private static void Execute(
        Compilation sourceLeft,
        IEnumerable<Target> sourceRight,
        SourceProductionContext spc)
    {
        foreach (var target in sourceRight.WhereNotNull())
        {
            var sb = new StringBuilder();

            foreach (var typeSymbol in target.Compositions)
            {
                var impl =
                    $@"""
                    namespace {target.ClassSymbol.ContainingNamespace.ToDisplayString()}
                    {{
                        partial {target.ClassSymbol.Name} : {typeSymbol.ToDisplayString()}
                        {{
                        }}
                    }}
                    """;
                
                spc.AddSource($"{target.ClassSymbol.ToDisplayString()}.{typeSymbol.ToDisplayString()}", impl);
            }
        }
    }

    private record Target(
        INamedTypeSymbol ClassSymbol,
        string FieldName,
        IReadOnlyList<INamedTypeSymbol> Compositions);
}