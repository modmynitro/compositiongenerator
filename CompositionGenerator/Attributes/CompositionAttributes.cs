﻿namespace CompositionGenerator.Attributes;

internal static class CompositionAttributes
{
    internal const string Attribute = 
        """
        using System;

        namespace CompositionGenerator.Attributes
        {
            [AttributeUsage(AttributeTargets.Field)]
            internal class CompositionAttribute<TInterface> : Attribute
            {
            }
        }
        """;
}