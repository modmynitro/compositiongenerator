﻿namespace CompositionGenerator.Extensions;

internal static class EnumerableExtensions
{
    public static IEnumerable<TElement> WhereNotNull<TElement>(this IEnumerable<TElement?> elements)
    {
        return elements.Where(element => element is not null).Cast<TElement>();
    }
}