﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CompositionGenerator.Extensions;

public static class ClassDeclarationSyntaxExtensions
{
    /// <summary>
    /// Checks if the class is marked as partial.
    /// </summary>
    /// <param name="classDeclarationSyntax">The class. Must not be <see langword="null"/>.</param>
    /// <returns><see langword="true"/> if marked as partial, otherwise <see langword="false"/>.</returns>
    /// <exception cref="ArgumentNullException">If <paramref name="classDeclarationSyntax"/> is <see langword="null"/>.</exception>
    public static bool IsPartial(this ClassDeclarationSyntax classDeclarationSyntax)
    {
        return classDeclarationSyntax.Modifiers.Any(m => m.IsKind(SyntaxKind.PartialKeyword));
    }
    
    public static string? GetNameSpace(this ClassDeclarationSyntax classDeclarationSyntax)
    {
        var parent = classDeclarationSyntax.TryGetParent<NamespaceDeclarationSyntax>();

        while (parent is not null)
        {
            return parent.Name.ToString();
        }

        return null;
    }
}