﻿using CompositionGenerator.SourceGenerators;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using VerifyXunit;

namespace CompositionGeneratorTests;

public static class TestHelper
{
    public static Task Verify(string source)
    {
        // Parse the provided string into a C# syntax tree
        var syntaxTree = CSharpSyntaxTree.ParseText(source);

        //Load project libraries
        var references = AppDomain.CurrentDomain
            .GetAssemblies()
            .Where(a => !a.IsDynamic)
            .Select(a => a.Location)
            .Where(s => !string.IsNullOrEmpty(s))
            .Where(s => !s.Contains("nunit"))
            .Select(s => MetadataReference.CreateFromFile(s))
            .ToList();
        
        // Create a Roslyn compilation for the syntax tree.
        var compilation = CSharpCompilation.Create(
            assemblyName: "Tests",
            references: references,
            syntaxTrees: new[] { syntaxTree },
            options: new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));
        
        // Create an instance of our EnumGenerator incremental source generator
        var generator = new CompositionSourceGenerator();

        // The GeneratorDriver is used to run our generator against a compilation
        GeneratorDriver driver = CSharpGeneratorDriver.Create(generator);

        // Run the source generator!
        driver = driver.RunGeneratorsAndUpdateCompilation(
            compilation,
            out var newCompilation,
            out var generatorDiagnostics);

        var diagnostics = newCompilation.GetDiagnostics();

        if (diagnostics.Length != 0)
            throw new InvalidOperationException();

        // Use verify to snapshot test the source generator output!
        return Verifier.Verify(driver);
    }
}