﻿using VerifyXunit;
using Xunit;

namespace CompositionGeneratorTests;

[UsesVerify] // 👈 Adds hooks for Verify into XUnit
public class CompositionSourceGeneratorTest
{
    [Fact]
    public Task GeneratesEnumExtensionsCorrectly()
    {
        CompositionGenerator.TestHelper.EnsureLoaded();
        
        // The source code to test
        var source =
            """
            using CompositionGenerator.Attributes;
            
            public interface ITest
            {
                int Test();
            }
            
            public class TestImplementation : ITest
            {
                public int Test()
                {
                    return 4711;
                }
            }

            public partial class Composition
            {
                [CompositionAttribute<ITest>]
                private readonly TestImplementation _composition = new();
            }
            """;

        // Pass the source code to our helper and snapshot test the output
        return TestHelper.Verify(source);
    }
}